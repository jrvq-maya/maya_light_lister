# -*- coding: UTF-8 -*-
"""
Author: Jaime Rivera
File: lister_widget.py
Date: 2019.06.27
Revision: 2020.01.12
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief:

"""

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'


from PySide2 import QtWidgets
from PySide2 import QtGui
from PySide2 import QtCore

import maya_specifics


# -------------------------------- CONSTANTS -------------------------------- #

# Color-related
NO_COLOR = None
DARK_GREEN = (31, 51, 36)
DARK_BLUE = (35, 42, 62)
DARK_YELLOW = (50, 50, 5)
DARK_ORANGE = (60, 40, 10)
DARK_CYAN = (5, 50, 50)

# Widget / grid distribution
HEADER_MARGIN = 20
MARGIN = 15

# Table distribution
INITIAL_ROW_HEIGHT = 45

# Fonts
TABLE_FONT = QtGui.QFont('arial', 10, 80)
TABLE_SMALL_FONT = QtGui.QFont('arial', 8, 50)
# Objects to filter
INVALID_OBJECT_TYPES = ['blocker', 'decay']


# -------------------------------- MAIN WIDGET -------------------------------- #

class ListerWidget(QtWidgets.QWidget):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)

        # Window properties
        self.setMinimumSize(800, 150)
        self.setWindowTitle('Light lister')
        self.setWindowIcon(QtGui.QIcon(':/render_spotLight.png'))

        # Grid distribution
        self.grid = QtWidgets.QGridLayout()
        self.grid.setContentsMargins(MARGIN, HEADER_MARGIN, MARGIN, MARGIN)
        self.grid.setHorizontalSpacing(MARGIN)
        self.setLayout(self.grid)

        # Main label
        lights_label = QtWidgets.QLabel('All lights')
        lights_label.setFont(QtGui.QFont('arial', 14))
        self.grid.addWidget(lights_label, 0, 0, 1, 3)

        # Lights table
        self.row_height = INITIAL_ROW_HEIGHT
        self.lights_table = QtWidgets.QTableWidget()
        self.lights_table.setSortingEnabled(True)
        self.lights_table.setIconSize(QtCore.QSize(40, 40))
        self.lights_table.setAlternatingRowColors(True)
        self.lights_table.setAlternatingRowColors(True)
        self.grid.addWidget(self.lights_table, 1, 0, 1, 9)

        # Refresh btn
        self.refresh_btn = QtWidgets.QPushButton(' Refresh table')
        self.refresh_btn.setFixedSize(140, 40)
        self.refresh_btn.setFont(TABLE_FONT)
        self.refresh_btn.setIcon(QtGui.QIcon(':/refresh.png'))
        self.refresh_btn.setIconSize(QtCore.QSize(30, 30))
        self.grid.addWidget(self.refresh_btn, 5, 0, 1, 1)

        # Slider
        slider_label = QtWidgets.QLabel('Row height:')
        slider_label.setFont(QtGui.QFont('arial', 12))
        slider_label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.grid.setRowMinimumHeight(5, 50)
        self.grid.addWidget(slider_label, 5, 7, 1, 1)

        self.slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.slider.setMinimum(35)
        self.slider.setMaximum(70)
        self.slider.setValue(INITIAL_ROW_HEIGHT)
        self.slider.setMaximumWidth(200)
        self.grid.addWidget(self.slider, 5, 8, 1, 1)

        # Setup
        self.make_connections()
        self.establish_columns()
        self.populate_table()
        self.show()

    def make_connections(self):
        self.lights_table.cellClicked.connect(self.cell_was_clicked)
        self.refresh_btn.clicked.connect(self.refresh_table)
        self.slider.valueChanged.connect(self.change_row_height)

    def establish_columns(self):

        maya_columns_data = ['Transform', 'Shape', 'Group', 'Node type', 'Visibility',
                             'Rename', 'Color', 'Use color temp.', 'Color temp.',
                             'Intensity', 'Exposure', 'Samples', 'Cast shadows',
                             'Diffuse', 'Specular', 'SSS', 'Indirect', 'Volume']

        for _ in maya_columns_data:
            self.lights_table.insertColumn(0)
        self.lights_table.setHorizontalHeaderLabels(maya_columns_data)

    def resize_table(self, resize_window=False):
        header = self.lights_table.horizontalHeader()
        total_columns = self.lights_table.columnCount()
        total_rows = self.lights_table.rowCount()

        # Establishing height for rows
        for row in range(total_rows):
            self.lights_table.setRowHeight(row, self.row_height)

        # Resize mode for columns
        for i in range(0, 13):
            header.setSectionResizeMode(i, QtWidgets.QHeaderView.ResizeToContents)
        for i in range(13, total_columns):
            header.setSectionResizeMode(i, QtWidgets.QHeaderView.Fixed)
            self.lights_table.setColumnWidth(i, 65)

        header.setSectionResizeMode(4, QtWidgets.QHeaderView.Fixed)
        self.lights_table.setColumnWidth(4, 55)

        if resize_window:
            # TOTAL SIZE
            total_width = sum([header.sectionSize(i) for i in range(0, total_columns)])
            total_height = self.row_height * total_rows
            total_height = total_height if total_height < 650 else 650
            self.resize(total_width + 80, total_height + 145)

        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)

    def change_row_height(self, new_value):
        self.row_height = new_value
        self.resize_table()

    def populate_table(self):

        transform_icon = QtGui.QIcon(':/out_transform.png')
        transform_icon.addPixmap(':/out_transform.png', QtGui.QIcon.Disabled)

        all_lights = maya_specifics.get_all_lights()
        for light in all_lights:

            # Filtering
            light_type = maya_specifics.get_node_type(light)
            if any(t in light_type.lower() for t in INVALID_OBJECT_TYPES):
                continue

            # New row
            row = self.lights_table.rowCount()
            self.lights_table.insertRow(row)
            column = 0

            # ----- WIDGETS PER ROW -----#

            # TRANSFORM
            transform = maya_specifics.get_the_transform(light)
            transform_widget = QtWidgets.QTableWidgetItem(transform + ' ')
            transform_widget.setIcon(transform_icon)
            transform_widget.setFont(TABLE_FONT)
            transform_widget.setFlags(QtCore.Qt.ItemIsEditable)
            transform_widget.setForeground(QtGui.QBrush(QtCore.Qt.white))
            self.lights_table.setItem(row, column, transform_widget)

            # SHAPE
            column += 1
            shape_widget = QtWidgets.QTableWidgetItem(' ' + light + ' ')
            shape_widget.setFont(TABLE_FONT)
            shape_widget.setFlags(QtCore.Qt.ItemIsEditable)
            shape_widget.setForeground(QtGui.QBrush(QtCore.Qt.white))
            self.lights_table.setItem(row, column, shape_widget)

            # GROUP (MAYA / ARNOLD)
            column += 1
            group = ' Maya ' if not maya_specifics.get_node_type(light).startswith('ai') else ' Arnold '
            group_widget = QtWidgets.QTableWidgetItem(group)
            group_widget.setFont(TABLE_FONT)
            group_widget.setFlags(QtCore.Qt.ItemIsEditable)
            group_widget.setForeground(QtGui.QBrush(QtCore.Qt.white))
            self.lights_table.setItem(row, column, group_widget)

            # NODE TYPE
            column += 1
            type = maya_specifics.get_node_type(light)
            type_widget = QtWidgets.QTableWidgetItem(' ' + type + ' ')
            type_widget.setFont(TABLE_SMALL_FONT)
            type_widget.setFlags(QtCore.Qt.ItemIsEditable)
            type_widget.setForeground(QtGui.QBrush(QtCore.Qt.white))
            type_widget.setIcon(self.get_light_icon(type))
            self.lights_table.setItem(row, column, type_widget)

            # VISIBILITY
            column += 1
            visibility = maya_specifics.get_an_attribute(transform, 'visibility')
            if visibility is not None:
                vis_btn = CustomOnOffButton(light)
                self.lights_table.setCellWidget(row, column, vis_btn)
            else:
                self.establish_empty_cell(row, column)

            # RENAME QLINE
            column += 1
            name_qline = QtWidgets.QLineEdit()
            name_qline.setPlaceholderText('Enter new name')
            name_qline.setFont(TABLE_FONT)
            self.lights_table.setCellWidget(row, column, name_qline)
            name_qline.editingFinished.connect(self.rename_light)

            # COLOR PICK BUTTON
            column += 1
            color_value = maya_specifics.get_an_attribute(light, 'color')
            if color_value is not None:
                color_btn = CustomColorButton(light)
                self.lights_table.setCellWidget(row, column, color_btn)
            else:
                self.establish_empty_cell(row, column)

            # COLOR TEMPERATURE CHECKBOX
            column += 1
            temp_bool = maya_specifics.get_an_attribute(light, 'aiUseColorTemperature')
            if temp_bool is not None:
                color_temp_check = CustomCheckBox(light, 'aiUseColorTemperature', 'Use color temp')
                self.lights_table.setCellWidget(row, column, color_temp_check)
            else:
                self.establish_empty_cell(row, column)

            # COLOR TEMERATURE SPINBOX
            column += 1
            temp = maya_specifics.get_an_attribute(light, 'aiColorTemperature')
            if temp is not None:
                colortemp_spin = CustomSpinbox(light, 'aiColorTemperature', 1000, 15000, 100, NO_COLOR)
                self.lights_table.setCellWidget(row, column, colortemp_spin)
            else:
                self.establish_empty_cell(row, column)

            if temp_bool is not None:
                color_temp_check.append_related_widget(color_btn, False)
                color_temp_check.append_related_widget(colortemp_spin, True)

            # INTENSITY SPINBOX
            column += 1
            intensity = maya_specifics.get_an_attribute(light, 'intensity')
            if intensity is not None:
                intensity_spin = CustomDoubleSpinbox(light, 'intensity', 0, 500, 0.5, DARK_YELLOW)
                self.lights_table.setCellWidget(row, column, intensity_spin)
            else:
                self.establish_empty_cell(row, column)

            # EXPOSURE SPINBOX
            column += 1
            exposure = maya_specifics.get_an_attribute(light, 'aiExposure')
            if exposure is not None:
                exposure_spin = CustomDoubleSpinbox(light, 'aiExposure', 0, 100, 0.5, DARK_ORANGE)
                self.lights_table.setCellWidget(row, column, exposure_spin)
            else:
                self.establish_empty_cell(row, column)

            # SAMPLES SPINBOX
            column += 1
            samples = maya_specifics.get_an_attribute(light, 'aiSamples')
            if samples is not None:
                samples_spin = CustomSpinbox(light, 'aiSamples', 0, 20, 2, DARK_CYAN)
                self.lights_table.setCellWidget(row, column, samples_spin)
            else:
                self.establish_empty_cell(row, column)

            # SHADOWS CHECKBOX
            column += 1
            shadows_bool = maya_specifics.get_an_attribute(light, 'aiCastShadows')
            if shadows_bool is not None:
                shadows = CustomCheckBox(light, 'aiCastShadows', 'Cast shadows')
                self.lights_table.setCellWidget(row, column, shadows)
            else:
                self.establish_empty_cell(row, column)

            # DIFFUSE SPINBOX
            column += 1
            diffuse = maya_specifics.get_an_attribute(light, 'aiDiffuse')
            if diffuse is not None:
                diffuse_spin = CustomDoubleSpinbox(light, 'aiDiffuse', 0, 1, 0.1, DARK_GREEN)
                self.lights_table.setCellWidget(row, column, diffuse_spin)
            else:
                self.establish_empty_cell(row, column)

            # SPECULAR SPINBOX
            column += 1
            specular = maya_specifics.get_an_attribute(light, 'aiSpecular')
            if specular is not None:
                specular_spin = CustomDoubleSpinbox(light, 'aiSpecular', 0, 1, 0.1, DARK_GREEN)
                self.lights_table.setCellWidget(row, column, specular_spin)
            else:
                self.establish_empty_cell(row, column)

            # SSS SPINBOX
            column += 1
            sss = maya_specifics.get_an_attribute(light, 'aiSss')
            if sss is not None:
                sss_spin = CustomDoubleSpinbox(light, 'aiSss', 0, 1, 0.1, DARK_GREEN)
                self.lights_table.setCellWidget(row, column, sss_spin)
            else:
                self.establish_empty_cell(row, column)

            # INDIRECT SPINBOX
            column += 1
            indirect = maya_specifics.get_an_attribute(light, 'aiIndirect')
            if indirect is not None:
                indirect_spin = CustomDoubleSpinbox(light, 'aiIndirect', 0, 1, 0.1, DARK_GREEN)
                self.lights_table.setCellWidget(row, column, indirect_spin)
            else:
                self.establish_empty_cell(row, column)

            # VOLUME SPINBOX
            column += 1
            volume = maya_specifics.get_an_attribute(light, 'aiVolume')
            if volume is not None:
                volume_spin = CustomDoubleSpinbox(light, 'aiVolume', 0, 1, 0.1, DARK_GREEN)
                self.lights_table.setCellWidget(row, column, volume_spin)
            else:
                self.establish_empty_cell(row, column)

        self.lights_table.sortByColumn(2, QtCore.Qt.AscendingOrder)
        self.resize_table(resize_window=True)

    def rename_light(self):
        qline = self.sender()
        postitionOfWidget = qline.pos()
        index = self.lights_table.indexAt(postitionOfWidget)
        row = index.row()

        old_name = self.lights_table.item(row, 0).text()
        new_name = qline.text()
        if not new_name:
            return

        qline.setText('')
        maya_specifics.rename_something(old_name, new_name)

        self.lights_table.item(row, 0).setText(new_name)
        self.lights_table.item(row, 1).setText(new_name + 'Shape')

        for c in range(0, self.lights_table.columnCount()):
            w = self.lights_table.cellWidget(row, c)
            if isinstance(w, AbstractSpecialWidget):
                w.change_associated_object(new_name + 'Shape')

    def get_light_icon(self, light_type):

        type_icon = QtGui.QIcon()

        icons_dict = {'area' : 'render_areaLight.png',
                      'directional' : 'render_directionalLight.png',
                      'spot' : 'spotlight.png',
                      'point' : 'pointlight.png',
                      'ambient' : 'render_ambientLight.png',
                      'sky' : 'render_envSky.png'}

        for type in icons_dict:
            if type in light_type.lower():
                type_icon.addPixmap(':/' + icons_dict[type])
                type_icon.addPixmap(':/' + icons_dict[type], QtGui.QIcon.Disabled)
                break

        return type_icon

    def establish_empty_cell(self, row, column):

        cell_widget = QtWidgets.QTableWidgetItem()
        brush = QtGui.QBrush(QtCore.Qt.darkRed, QtCore.Qt.BDiagPattern)
        cell_widget.setBackground(brush)
        cell_widget.setFlags(QtCore.Qt.ItemIsEditable)

        self.lights_table.setItem(row, column, cell_widget)

    def cell_was_clicked(self, row, column):
        if column in [0, 1]:
            obj = self.lights_table.item(row, column).text().strip()
            maya_specifics.select_something(obj)

    def refresh_table(self):
        self.lights_table.setRowCount(0)
        self.lights_table.setColumnCount(0)
        self.establish_columns()
        self.populate_table()


# -------------------------------- AUX WIDGETS -------------------------------- #

class AbstractSpecialWidget(object):
    def __init__(self, maya_object, maya_attr):
        self.associated_object = maya_object
        self.associated_attribute = maya_attr

    def change_associated_object(self, new_object):
        self.associated_object = new_object

    def get_associated_attr(self):
        return maya_specifics.get_an_attribute(self.associated_object, self.associated_attribute)

    def modify_associated_attr(self, new_value):
        maya_specifics.set_attr(self.associated_object, self.associated_attribute, new_value)


class CustomCheckBox(QtWidgets.QWidget, AbstractSpecialWidget):
    def __init__(self, maya_object, maya_attr, label):
        QtWidgets.QWidget.__init__(self)
        AbstractSpecialWidget.__init__(self, maya_object, maya_attr)

        self.related_widgets = dict()

        self.checkbox = QtWidgets.QCheckBox(label)
        self.checkbox.setStyleSheet('QCheckBox::indicator:checked {image: url(:/checkboxOn.png);}'
                                    'QCheckBox::indicator:unchecked {image: url(:/checkboxOff.png);}')
        qhboxlayout = QtWidgets.QHBoxLayout(self)
        qhboxlayout.addWidget(self.checkbox)
        qhboxlayout.setAlignment(QtCore.Qt.AlignCenter)

        initial_value = self.get_associated_attr()
        self.checkbox.setChecked(initial_value)

        self.check_connected()

        self.checkbox.toggled.connect(self.modify_associated_attr)
        self.checkbox.toggled.connect(self.toggle_related_widgets)

    def append_related_widget(self, widget, inverse):
        self.related_widgets[widget] = inverse
        self.toggle_related_widgets(self.get_associated_attr())

    def toggle_related_widgets(self, new_state):
        if self.related_widgets:
            for w in self.related_widgets:
                if self.related_widgets[w]:
                    w.check_connected(force_lock=not new_state)
                else:
                    w.check_connected(force_lock=new_state)
                if isinstance(w, CustomColorButton):
                    w.set_forbidden(new_state)

    def check_connected(self, force_lock=False):
        connected = maya_specifics.check_for_connections(self.associated_object, self.associated_attribute)
        if connected or force_lock:
            self.setDisabled(True)
            self.checkbox.setStyleSheet('QCheckBox::indicator:checked {image: url(:/lockGeneric.png);}'
                                        'QCheckBox::indicator:unchecked {image: url(:/lockGeneric.png);}')
        else:
            self.setEnabled(True)


class CustomColorButton(QtWidgets.QPushButton, AbstractSpecialWidget):
    def __init__(self, maya_object):
        QtWidgets.QPushButton.__init__(self)
        AbstractSpecialWidget.__init__(self, maya_object, 'color')

        initial_color = self.get_associated_attr()[0]
        self.r, self.g, self.b = (c * 255 for c in initial_color)
        self.setStyleSheet(
            'QPushButton{{color:white;background: rgb({0},{1},{2});}} QToolTip{{color:none;background:none;}}'
            ''.format(self.r, self.g, self.b))

        self.setToolTip('Click on this button to modify the light\'s <b>color</b> attribute')

        self.check_connected()

        self.clicked.connect(self.change_color)

    def check_connected(self, force_lock=False):
        connected = maya_specifics.check_for_connections(self.associated_object, self.associated_attribute)
        if connected or force_lock:
            self.setDisabled(True)
            lock_icon = QtGui.QIcon(':/lockGeneric.png')
            lock_icon.addPixmap(':/lockGeneric.png', QtGui.QIcon.Disabled)
            self.setIconSize(QtCore.QSize(25, 25))
            self.setIcon(lock_icon)
        else:
            self.setEnabled(True)
            self.setIcon(QtGui.QIcon())

    def set_forbidden(self, force_forbidden):
        connected = maya_specifics.check_for_connections(self.associated_object, self.associated_attribute)
        if connected:
            return

        if force_forbidden:
            self.setDisabled(True)
            lock_icon = QtGui.QIcon(':/error.png')
            lock_icon.addPixmap(':/error.png', QtGui.QIcon.Disabled)
            self.setIconSize(QtCore.QSize(25, 25))
            self.setIcon(lock_icon)
        else:
            self.setEnabled(True)
            self.setIcon(QtGui.QIcon())

    def change_color(self):
        c_dialog = QtWidgets.QColorDialog()
        new_color = c_dialog.getColor(QtGui.QColor(self.r, self.g, self.b))

        if not new_color.isValid():
            return

        self.r, self.g, self.b = new_color.red(), new_color.green(), new_color.blue()
        maya_specifics.set_color_attr(self.associated_object, new_color)

        self.setStyleSheet(
            'QPushButton{{color:white;background: rgb({0},{1},{2});}} QToolTip{{color:none;background:none;}}'
            ''.format(self.r, self.g, self.b))


class CustomOnOffButton(QtWidgets.QPushButton, AbstractSpecialWidget):
    def __init__(self, maya_object):
        QtWidgets.QPushButton.__init__(self)
        AbstractSpecialWidget.__init__(self, maya_object, 'visibility')

        self.setMaximumWidth(55)

        self.setFont(TABLE_FONT)
        self.setText('ON')
        self.setStyleSheet(
            'QPushButton{{color:white;background: rgb{COLOR};}} QToolTip{{color:none;background:none;}}'
            ''.format(COLOR=DARK_BLUE))
        if not self.get_associated_attr():
            self.setText('OFF')
            self.setStyleSheet(
                'QPushButton{{color:red;background: rgb{COLOR};}} QToolTip{{color:none;background:none;}}'
                ''.format(COLOR=DARK_BLUE))

        self.setToolTip('Click on this button to toggle the light\'s shape <b>visibility</b> attribute')

        self.check_connected()

        self.clicked.connect(self.switch_on_off)

    def check_connected(self, force_lock=False):
        connected = maya_specifics.check_for_connections(self.associated_object, self.associated_attribute)
        if connected or force_lock:
            self.setDisabled(True)
            lock_icon = QtGui.QIcon(':/lockGeneric.png')
            lock_icon.addPixmap(':/lockGeneric.png', QtGui.QIcon.Disabled)
            self.setIconSize(QtCore.QSize(25, 25))
            self.setIcon(lock_icon)
        else:
            self.setEnabled(True)
            self.setIcon(QtGui.QIcon())

    def switch_on_off(self, modify=True):
        state = not self.get_associated_attr()
        self.modify_associated_attr(state)
        if state:
            self.setText('ON')
            self.setStyleSheet(
                'QPushButton{{color:white;background: rgb{COLOR};}} QToolTip{{color:none;background:none;}}'
                ''.format(COLOR=DARK_BLUE))
        else:
            self.setText('OFF')
            self.setStyleSheet(
                'QPushButton{{color:red;background: rgb{COLOR};}} QToolTip{{color:none;background:none;}}'
                ''.format(COLOR=DARK_BLUE))


class CustomSpinbox(QtWidgets.QSpinBox, AbstractSpecialWidget):
    def __init__(self, maya_object, maya_attr, min, max, step, color):
        QtWidgets.QSpinBox.__init__(self)
        AbstractSpecialWidget.__init__(self, maya_object, maya_attr)

        initial_value = int(self.get_associated_attr())
        self.setMinimum(min)
        self.setMaximum(max)
        self.setSingleStep(step)
        self.setValue(initial_value)

        self.setToolTip('Change this value to modify the <b>{}</b> attribute of the light object'
                        ''.format(self.associated_attribute))

        if color:
            self.setStyleSheet(
                'QSpinBox{{background: rgb{COLOR};}} QToolTip{{color:none;background:none;}}'.format(COLOR=color))

        self.check_connected()

        self.valueChanged.connect(self.modify_associated_attr)

    def check_connected(self, force_lock=False):
        connected = maya_specifics.check_for_connections(self.associated_object, self.associated_attribute)
        if connected or force_lock:
            self.setDisabled(True)
        else:
            self.setEnabled(True)

    def wheelEvent(self, event):
        pass


class CustomDoubleSpinbox(QtWidgets.QDoubleSpinBox, AbstractSpecialWidget):
    def __init__(self, maya_object, maya_attr, min, max, step, color):
        QtWidgets.QDoubleSpinBox.__init__(self)
        AbstractSpecialWidget.__init__(self, maya_object, maya_attr)

        initial_value = self.get_associated_attr()
        self.setMinimum(min)
        self.setMaximum(max)
        self.setSingleStep(step)
        self.setDecimals(3)
        self.setValue(initial_value)

        self.setToolTip('Change this value to modify the <b>{}</b> attribute of the light object'
                        ''.format(self.associated_attribute))

        if color:
            self.setStyleSheet('QDoubleSpinBox{{background: rgb{COLOR};}} QToolTip{{color:none;background:none;}}'
                               ''.format(COLOR=color))

        self.check_connected()

        self.valueChanged.connect(self.modify_associated_attr)

    def check_connected(self, force_lock=False):
        connected = maya_specifics.check_for_connections(self.associated_object, self.associated_attribute)
        if connected or force_lock:
            self.setDisabled(True)
        else:
            self.setEnabled(True)

    def wheelEvent(self, event):
        pass
